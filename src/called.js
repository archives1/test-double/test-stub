


function child() {
  console.log('does child');
}

function parent() {
  console.log('does parent');
  exportFunctions.child();
}

const exportFunctions = {
  parent,
  child
};

module.exports = exportFunctions;