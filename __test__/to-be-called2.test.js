

const myObj = {
    parent() {
      console.log('does parent');
      myObj.child()
    },

    child() {
        console.log('does child');
    },
      
};

describe('Stub',()=>{

    test('stub .toBeCalled()', () => {
        const stub = jest.fn();
        stub();
        expect(stub).toBeCalled();
    });
    test('spyOn .toBeCalled()', () => {
        const somethingSpy = jest.spyOn(myObj, 'child');
        myObj.parent();
        expect(somethingSpy).toBeCalled();
    });

});