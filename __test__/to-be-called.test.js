

const myObj = {
    doSomething() {
      console.log('does something');
    }
};

describe('Stub',()=>{

    test('stub .toBeCalled()', () => {
        const stub = jest.fn();
        stub();
        expect(stub).toBeCalled();
    });
    test('spyOn .toBeCalled()', () => {
        const somethingSpy = jest.spyOn(myObj, 'doSomething');
        myObj.doSomething();
        expect(somethingSpy).toBeCalled();
    });

});